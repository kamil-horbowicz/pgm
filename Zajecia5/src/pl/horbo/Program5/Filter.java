package pl.horbo.Program5;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import pl.horbo.Program4.Filter.AvgFilter;

public class Filter {

	private Mat source;
	private Mat ycbcr;
	private Mat rgbr;
	private Mat rgbg;
	private Mat rgbb;

	public Filter(Mat source) {
		this.source = source;
		this.ycbcr = source.clone();

		Imgproc.cvtColor(source, ycbcr, Imgproc.COLOR_BGR2YCrCb);
		transform();
	}

	private void transform() {

		AvgFilter a1 = new AvgFilter(source);
		a1.transform(0);
		// MedFilter a2 = new MedFilter(source);
		// a2.transformColor(0);
		//
		// rgbr = a2.getTarget().clone();
		// a2.transformColor(1);
		// rgbb = a2.getTarget().clone();
		// a2.transformColor(2);
		// rgbg = a2.getTarget().clone();
		//
		// a2.setSource(ycbcr);
		// a2.transformColor(0);
		// ycbcr = a2.getTarget().clone();

		rgbr = a1.getTarget().clone();
		a1.transform(1);
		rgbb = a1.getTarget().clone();
		a1.transform(2);
		rgbg = a1.getTarget().clone();

		a1.setSource(ycbcr);
		a1.transform(0);
		ycbcr = a1.getTarget().clone();

	}

	public Mat getRgbr() {
		return rgbr;
	}

	public Mat getRgbg() {
		return rgbg;
	}

	public Mat getRgbb() {
		return rgbb;
	}

	public Mat getYCrBr() {
		return ycbcr;
	}

}
