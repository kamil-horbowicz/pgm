package pl.horbo.Program5;

import java.awt.EventQueue;

import org.opencv.core.Core;

import pl.horbo.Program5.Detect.GreenPeppers;
import pl.horbo.Program5.Detect.RedEye;
import pl.horbo.Program5.Detect.Skin;
import pl.horbo.Tools.Pictures;
import pl.horbo.Tools.PreviewWindow;

public class Main {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		RedEye redEye = new RedEye(Pictures.FACE1.getMat());
		Skin skin = new Skin(Pictures.FACE.getMat());
		GreenPeppers greenPeppers = new GreenPeppers(Pictures.PEPPERS.getMat());
		Filter filter = new Filter(Pictures.PEPPERS.getMat());
		//
		// EventQueue.invokeLater(() -> new
		// PreviewWindow(greenPeppers.getResult(), "Skin"));
		// EventQueue.invokeLater(() -> new PreviewWindow(skin.getResult(),
		// "Skin"));
		// EventQueue.invokeLater(() -> new PreviewWindow(redEye.getResult(),
		// "Red eye"));
		EventQueue.invokeLater(() -> new PreviewWindow(filter.getRgbr(), "Peppers Red"));
		EventQueue.invokeLater(() -> new PreviewWindow(filter.getRgbg(), "Peppers Green"));
		EventQueue.invokeLater(() -> new PreviewWindow(filter.getRgbb(), "Peppers Blue"));
		EventQueue.invokeLater(() -> new PreviewWindow(filter.getYCrBr(), "YCrBr"));
	}

}
