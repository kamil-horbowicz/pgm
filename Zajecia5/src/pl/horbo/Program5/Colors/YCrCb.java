package pl.horbo.Program5.Colors;

public class YCrCb {

	private double y;
	private double cb;
	private double cr;

	private RGB rgb;

	public YCrCb(double y, double cb, double cr) {
		this.y = y;
		this.cb = cb;
		this.cr = cr;
	}

	public YCrCb(RGB rgb) {
		this.rgb = rgb;
		convertFromRGB();
	}

	private void convertFromRGB() {
		y = (0.257 * rgb.getR() + 0.504 * rgb.getG() + 0.098 * rgb.getB()) + 16;
		cb = (-0.148 * rgb.getR() - 0.291 * rgb.getG() + 0.439 * rgb.getB()) + 128;
		cr = (0.439 * rgb.getR() - 0.368 * rgb.getG() - 0.071 * rgb.getB()) + 128;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getCb() {
		return cb;
	}

	public void setCb(double cb) {
		this.cb = cb;
	}

	public double getCr() {
		return cr;
	}

	public void setCr(double cr) {
		this.cr = cr;
	}

	public RGB getRgb() {
		return rgb;
	}

	public void setRgb(RGB rgb) {
		this.rgb = rgb;
		convertFromRGB();
	}

	@Override
	public String toString() {
		return String.format("[%f, %f, %f]", y, cr, cb);
	}
}
