package pl.horbo.Program5.Colors;

import pl.horbo.Tools.HMath;

public class HLS {
	private double h;
	private double l;
	private double s;

	private RGB rgb;

	public HLS(double h, double l, double s) {
		this.h = h;
		this.l = l;
		this.s = s;
	}

	public HLS(double[] hls) {
		this.h = hls[0];
		this.l = hls[1];
		this.s = hls[2];
	}

	public HLS(RGB rgb) {
		this.rgb = rgb;
		convertToHLS();
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}

	public double getL() {
		return l;
	}

	public void setL(double l) {
		this.l = l;
	}

	public double getS() {
		return s;
	}

	public void setS(double s) {
		this.s = s;
	}

	public double[] getAsArray() {
		return new double[] { h, l, s };
	}

	private void convertToHLS() {
		l = l();
		s = s();
		h = h();
	}

	private double l() {
		return (HMath.max(rgb.getR(), rgb.getG(), rgb.getB()) + HMath.min(rgb.getR(), rgb.getG(), rgb.getB())) / 2;
	}

	private double s() {
		if (l < 0.5) {
			return (HMath.max(rgb.getR(), rgb.getG(), rgb.getB() - HMath.min(rgb.getR(), rgb.getG(), rgb.getB())))
					/ (HMath.max(rgb.getR(), rgb.getG(), rgb.getB() + HMath.min(rgb.getR(), rgb.getG(), rgb.getB())));
		} else {
			return (HMath.max(rgb.getR(), rgb.getG(), rgb.getB() - HMath.min(rgb.getR(), rgb.getG(), rgb.getB()))) / (2
					- (HMath.max(rgb.getR(), rgb.getG(), rgb.getB() + HMath.min(rgb.getR(), rgb.getG(), rgb.getB()))));
		}
	}

	private double h() {
		if (HMath.isMax(rgb.getR(), rgb.getR(), rgb.getG(), rgb.getB())) {
			return (60 * (rgb.getG() - rgb.getB())) / s;
		} else if (HMath.isMax(rgb.getG(), rgb.getR(), rgb.getG(), rgb.getB())) {
			return 120 + ((60 * (rgb.getG() - rgb.getB())) / s);
		} else {
			return 240 + ((60 * (rgb.getG() - rgb.getB())) / s);
		}
	}
}