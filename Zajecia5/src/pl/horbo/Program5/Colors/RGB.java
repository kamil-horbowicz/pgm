package pl.horbo.Program5.Colors;

public class RGB {
	private double r;
	private double g;
	private double b;

	private YCrCb yCrCb;

	public RGB(double r, double g, double b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public RGB(double[] rgb) {
		this.r = rgb[2];
		this.g = rgb[1];
		this.b = rgb[0];
	}

	public RGB(YCrCb yCrCb) {
		this.yCrCb = yCrCb;
		convertFromYCrCb();
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double[] getAsArray() {
		return new double[] { r, g, b };
	}

	private void convertFromYCrCb() {
		r = 1.164 * (yCrCb.getY() - 16) + 1.596 * (yCrCb.getCr() - 128);
		g = 1.164 * (yCrCb.getY() - 16) - 0.392 * (yCrCb.getCb() - 128) - 0.813 * (yCrCb.getCr() - 128);
		b = 1.164 * (yCrCb.getY() - 16) + 2.017 * (yCrCb.getCb() - 128);
	}

	public RGB getGrayScale() {
		double gray = 0.299 * r + 0.587 * g + 0.114 * b;
		return new RGB(gray, gray, gray);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("[%f, %f, %f]", r, g, b);
	}

}
