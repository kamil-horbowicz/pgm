package pl.horbo.Program5.Detect;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import pl.horbo.Program5.Colors.HLS;
import pl.horbo.Program5.Colors.RGB;

public class Skin {
	private Mat source;
	private Mat hlsMat;

	public Skin(Mat source) {
		this.source = source;
		this.hlsMat = new Mat(source.rows(), source.cols(), source.type());
		Imgproc.cvtColor(source, hlsMat, Imgproc.COLOR_BGR2HLS);
		transform();
	}

	public Mat getResult() {
		return source;
	}

	private void transform() {
		for (int i = 0; i < source.width(); i++) {
			for (int j = 0; j < source.height(); j++) {
				if (!isCondition(new HLS(hlsMat.get(j, i)))) {
					source.put(j, i, new RGB(0, 0, 0).getAsArray());
				}
			}
		}
	}

	private double lsRatio(HLS hls) {
		return hls.getL() / hls.getS();
	}

	private boolean isCondition(HLS hls) {
		return hls.getS() >= 50 && lsRatio(hls) > 0.5 && lsRatio(hls) < 3 && (hls.getH() <= 14 || hls.getH() >= 165);
	}
}
