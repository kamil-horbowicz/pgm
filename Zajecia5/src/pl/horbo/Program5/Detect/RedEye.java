package pl.horbo.Program5.Detect;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import pl.horbo.Program5.Colors.HLS;
import pl.horbo.Program5.Colors.RGB;

public class RedEye {

	private Mat source;
	private Mat hlsMat;

	public RedEye(Mat source) {
		this.source = source;
		this.hlsMat = new Mat(source.rows(), source.cols(), source.type());
		Imgproc.cvtColor(source, hlsMat, Imgproc.COLOR_BGR2HLS);
		transform();
	}

	public Mat getResult() {
		return source;
	}

	private void transform() {
		for (int i = 0; i < source.width(); i++) {
			for (int j = 0; j < source.height(); j++) {
				if (!isCondition(new HLS(hlsMat.get(j, i)))) {
					source.put(j, i, new RGB(source.get(j, i)).getGrayScale().getAsArray());
				}
			}
		}
	}

	private double lsRatio(HLS hls) {
		return hls.getL() / hls.getS();
	}

	private boolean isCondition(HLS hls) {
		return hls.getL() >= 64 && hls.getS() >= 100 && lsRatio(hls) > 0.5 && lsRatio(hls) < 1.5
				&& (hls.getH() <= 7 || hls.getH() >= 162);
	}
}
