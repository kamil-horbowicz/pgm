package pl.horbo.Program5.Detect;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import pl.horbo.Program5.Colors.HLS;
import pl.horbo.Program5.Colors.RGB;

public class GreenPeppers {

	private Mat source;
	private Mat hlsMat;
	private static final double sensitivity = 45;

	public GreenPeppers(Mat source) {
		this.source = source;
		this.hlsMat = new Mat(source.rows(), source.cols(), source.type());
		Imgproc.cvtColor(source, hlsMat, Imgproc.COLOR_BGR2HSV);
		transform();
	}

	private void transform() {
		for (int i = 0; i < source.width(); i++) {
			for (int j = 0; j < source.height(); j++) {
				if (!isCondition(new HLS(hlsMat.get(j, i)))) {
					source.put(j, i, new RGB(255, 255, 255).getAsArray());
				}
			}
		}
	}

	public Mat getResult() {
		return source;
	}

	private double lsRatio(HLS hls) {
		return hls.getL() / hls.getS();
	}

	private boolean isBetween(double value, double min, double max) {
		return value >= min && value <= max;
	}

	private boolean isCondition(HLS hsv) {
		return isBetween(hsv.getH(), 60 - sensitivity, 60 + sensitivity) && isBetween(hsv.getL(), 100, 255)
				&& isBetween(hsv.getS(), 50, 255);
	}
}
