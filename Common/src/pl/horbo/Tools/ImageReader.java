package pl.horbo.Tools;

import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class ImageReader {
	private Mat photo;
	private String path;

	public ImageReader(String path) {
		this.path = path;
		readPhoto();
	}

	public ImageReader() {
	}

	private void readPhoto() {
		photo = Highgui.imread(path);
	}

	public Mat getMat() {
		return photo;
	}

	public BufferedImage getPhoto() {
		return getPhoto(photo);
	}

	public BufferedImage getPhoto(Mat photo) {
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (photo.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = photo.channels() * photo.cols() * photo.rows();
		byte[] b = new byte[bufferSize];
		photo.get(0, 0, b);
		BufferedImage image = new BufferedImage(photo.cols(), photo.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;
	}

	public void displayImage(Mat photo) {
		displayImage(getPhoto(photo));
	}

	public void displayImage() {
		displayImage(getPhoto());
	}

	public void displayImage(Image image) {
		ImageIcon icon = new ImageIcon(image);
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout());
		frame.setSize(image.getWidth(null) + 50, image.getHeight(null) + 50);
		JLabel lbl = new JLabel();
		lbl.setIcon(icon);
		frame.add(lbl);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public ImageIcon getImageIcon(String path) {
		this.path = path;
		readPhoto();
		return new ImageIcon(getPhoto());
	}

	public int getWidth() {
		return photo.width();
	}

	public int getHeight() {
		return photo.height();
	}

}
