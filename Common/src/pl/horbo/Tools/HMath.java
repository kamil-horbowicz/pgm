package pl.horbo.Tools;

import java.util.Arrays;
import java.util.Random;

public class HMath {

	/**
	 * Metoda zwraca najwi�kszy element z zestawu.
	 * 
	 * @param elements
	 *            Zestaw danych.
	 * @return Najmniejszy element.
	 */
	@SafeVarargs
	public static <T extends Comparable<T>> T min(T... elements) {
		return Arrays.stream(elements).filter(o -> o != null).min((o1, o2) -> o1.compareTo(o2)).get();
	}

	public static double avg(double... elements) {
		double suma = 0;

		for (int i = 0; i < elements.length; i++) {
			suma += elements[i];
		}

		return suma / elements.length;
	}

	public static double[] getNormal(double avg, double std, int n) {
		double[] data = new double[n];

		Random r = new Random();
		for (int i = 0; i < n; i++) {
			data[i] = r.nextGaussian() * std + avg;
		}

		return data;
	}

	/**
	 * Metoda zwraca najwi�kszy element z zestawu.
	 * 
	 * @param elements
	 *            Zestaw danych.
	 * @return Najmniejszy element.
	 */
	@SafeVarargs
	public static <T extends Comparable<T>> T max(T... elements) {
		return Arrays.stream(elements).filter(o -> o != null).max((o1, o2) -> o1.compareTo(o2)).get();
	}

	/**
	 * Metoda zwracaj�ca czy element <b>current</b> jest najmniejszy w zestawie.
	 * 
	 * @param current
	 *            Element do sprawdzenia.
	 * @param all
	 *            Zestaw wszystkich element�w.
	 * @return <b>TRUE</b> je�li element <b>current</b> jest najmniejszy, FALSE
	 *         w przeciwnym wypadku.
	 */
	public static <T extends Comparable<T>> boolean isMin(T current, T... all) {
		return Arrays.stream(all).allMatch(t -> t.compareTo(current) >= 0);
	}

	/**
	 * Metoda zwracaj�ca czy element <b>current</b> jest najwi�kszy w zestawie.
	 * 
	 * @param current
	 *            Element do sprawdzenia.
	 * @param all
	 *            Zestaw wszystkich element�w.
	 * @return <b>TRUE</b> je�li element <b>current</b> jest najwi�kszy, FALSE w
	 *         przeciwnym wypadku.
	 */
	public static <T extends Comparable<T>> boolean isMax(T current, T... all) {
		return Arrays.stream(all).allMatch(t -> t.compareTo(current) <= 0);
	}

	public static boolean isMin(long current, long[] all) {
		return Arrays.stream(all).allMatch(t -> Long.compare(t, current) >= 0);
	}
}
