package pl.horbo.Tools;

import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import org.opencv.core.Mat;

public class PreviewWindow extends JFrame {

	private static final long serialVersionUID = 6333195190895296215L;

	private ImageIcon image;

	private PreviewWindow() {
		super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
	}

	public PreviewWindow(Mat display) {
		this();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		setSize(display.width(), display.height());
		image = new ImageIcon(new ImageReader().getPhoto(display));
		setVisible(true);
		build();
	}

	public PreviewWindow(Mat display, String title) {
		this(display);
		setTitle(title);
	}

	private void build() {
		JLabel label = new JLabel();
		label.setIcon(image);
		add(label);
	}

}
