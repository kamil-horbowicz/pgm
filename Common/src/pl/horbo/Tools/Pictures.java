package pl.horbo.Tools;

import org.opencv.core.Mat;

public enum Pictures {

	LENA("LENA_512.jpg"), FACE1("face1.jpg"), FACE("face.jpg"), PEPPERS("peppers.png"), KOBIETA("kobieta.jpg"), STATEK(
			"statek_640_505.jpg");

	private static final String dir = "D:\\Images";
	private final String name;
	private final ImageReaderWriter readerWriter;

	Pictures(String name) {
		this.name = name;
		this.readerWriter = new ImageReaderWriter(getPath());
	}

	public String getName() {
		return name;
	}

	public Mat getMat() {
		return readerWriter.readMatrix();
	}

	public String getPath() {
		return dir + "\\" + name;
	}

}
