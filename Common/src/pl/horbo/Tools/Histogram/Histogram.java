package pl.horbo.Tools.Histogram;

import org.jfree.data.statistics.HistogramDataset;
import org.opencv.core.Mat;

import pl.horbo.Tools.HMath;

public class Histogram {

	public static HistogramDataset get(Mat mat) {
		double[][] kolory = new double[mat.channels()][mat.cols() * mat.rows()];
		int a = 0;
		for (int i = 0; i < mat.cols(); i++) {
			for (int j = 0; j < mat.rows(); j++) {
				for (int color = 0; color < mat.channels(); color++) {
					kolory[color][a++] = mat.get(j, i)[color];
				}
			}
		}
		HistogramDataset dataSet = new HistogramDataset();
		for (int i = 0; i < mat.channels(); i++)
			dataSet.addSeries(i, kolory[i], 256);

		return dataSet;
	}

	public static HistogramDataset get(Mat mat, int color) {
		double[] kolory = new double[mat.cols() * mat.rows()];
		int a = 0;
		for (int i = 0; i < mat.cols(); i++) {
			for (int j = 0; j < mat.rows(); j++) {
				kolory[a++] = mat.get(j, i)[color];
			}
		}
		HistogramDataset dataSet = new HistogramDataset();
		dataSet.addSeries(0, kolory, 256);

		return dataSet;
	}

	public static HistogramDataset getGray(Mat mat) {
		double[] kolory = new double[mat.cols() * mat.rows()];
		int a = 0;

		for (int i = 0; i < mat.cols(); i++) {
			for (int j = 0; j < mat.rows(); j++) {
				kolory[a++] = HMath.avg(mat.get(j, i));
			}
		}

		HistogramDataset dataSet = new HistogramDataset();
		dataSet.addSeries(0, kolory, 256);

		return dataSet;
	}

	public static HistogramDataset get(double[] data, int bins) {
		HistogramDataset dataSet = new HistogramDataset();
		dataSet.addSeries(0, data, bins);

		return dataSet;
	}
}
