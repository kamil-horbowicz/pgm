package pl.horbo.Tools.Histogram;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;

public class HistogramWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9055154574079800693L;
	private ChartPanel chartPanel;
	private JFreeChart chart;

	public HistogramWindow(HistogramDataset dataSet) {

		super("Histogram");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		setVisible(true);
		chart = ChartFactory.createHistogram("Histogram", "Value", "Number", dataSet, PlotOrientation.VERTICAL, false,
				false, false);

		chartPanel = new ChartPanel(chart);

		XYPlot xyplot = (XYPlot) chart.getPlot();
		xyplot.setBackgroundPaint(Color.WHITE);
		xyplot.setDomainGridlinesVisible(false);
		xyplot.setRangeGridlinesVisible(false);
		XYBarRenderer xybarrenderer = (XYBarRenderer) xyplot.getRenderer();
		xybarrenderer.setShadowVisible(false);

		xybarrenderer.setBarPainter(new StandardXYBarPainter());

		chartPanel.setSize(800, 600);
		add(chartPanel);
		setSize(chartPanel.getSize());

	}

}
