package pl.horbo.Program6;

import java.awt.EventQueue;

import org.opencv.core.Core;

import pl.horbo.Tools.HMath;
import pl.horbo.Tools.Histogram.Histogram;
import pl.horbo.Tools.Histogram.HistogramWindow;

public class Main {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		// EventQueue.invokeLater(() -> new
		// HistogramWindow(Histogram.getGray(Pictures.KOBIETA.getMat())));
		EventQueue.invokeLater(() -> new HistogramWindow(Histogram.get(HMath.getNormal(0, 1, 500), 50)));
	}

}
