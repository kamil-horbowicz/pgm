package pl.horbo.Program2.Model;

import org.opencv.core.Mat;
import org.opencv.core.Range;

import pl.horbo.Program2.Model.ColorTransform.IColorTransform;
import pl.horbo.Program2.Model.ColorTransform.IPixelTransform;
import pl.horbo.Program2.Model.PositionTransform.IPositionTransform;
import pl.horbo.Tools.ImageReader;

public class ImageEditor {

	private Mat image;
	private Mat current = new Mat();
	private ImageReader reader;

	public ImageEditor(Mat image) {
		this.reader = new ImageReader();
		this.image = image;
	}

	public ImageEditor() {
	}

	public void transformPicture(IPixelTransform transform) {
		if (image == null) {
			image = reader.getMat();
		}
		current = new Mat(image.size(), image.type());
		for (int i = 0; i < image.width(); i++) {
			for (int j = 0; j < image.height(); j++) {
				current.put(j, i, transform.transform(image.get(j, i)));
			}
		}
	}

	public void transformPicture(IColorTransform transform) {
		if (image == null) {
			image = reader.getMat();
		}
		current = transform.transform(image);
	}

	public void transformPicture(IPositionTransform transform) {
		if (image == null) {
			image = reader.getMat();
		}
		current = transform.transform(image);
	}

	public void displayImage() {
		reader.displayImage(current);
	}

	public Mat getImage() {
		return image;
	}

	public void copyCurrentToOriginal() {
		image = new Mat(current, Range.all());
	}

	public void setImage(Mat image) {
		this.image = image;
	}

	public Mat getCurrentImage() {
		return current;
	}

	public void setCurrentImage(Mat current) {
		this.current = current;
	}

	public void setReader(ImageReader reader) {
		this.reader = reader;
	}

	public ImageReader getReader() {
		return reader;
	}

}
