package pl.horbo.Program2.Model.ColorTransform;

public class OutOfRangeException extends Exception {
	private static final long serialVersionUID = 4235755236519785039L;

	public OutOfRangeException(int value, int from, int to) {
		super(String.format("Value [%d] is out of range [%d,%d]", value, from, to));
	}
}