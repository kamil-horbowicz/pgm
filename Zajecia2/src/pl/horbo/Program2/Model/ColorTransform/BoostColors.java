package pl.horbo.Program2.Model.ColorTransform;

public class BoostColors implements IPixelTransform {

	private int w;

	public BoostColors(int w) throws OutOfRangeException {
		if (w < 0 || w > 255) {
			throw new OutOfRangeException(w, 0, 255);
		}
		this.w = w;
	}

	public BoostColors() {
	}

	@Override
	public double[] transform(double[] pixel) {
		return new double[] { Math.min(pixel[0] + w, 255), Math.min(pixel[1] + w, 255), Math.min(pixel[2] + w, 255) };
	}

}
