package pl.horbo.Program2.Model.ColorTransform;

public interface IPixelTransform {
	double[] transform(double[] pixel);
}
