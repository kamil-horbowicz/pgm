package pl.horbo.Program2.Model.ColorTransform;

public class Negative implements IPixelTransform {
	private static double[] result = new double[3];

	@Override
	public double[] transform(double[] pixel) {
		for (int i = 0; i < 3; i++) {
			result[i] = 255 - pixel[i];
		}
		return result;
	}
}
