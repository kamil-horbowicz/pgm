package pl.horbo.Program2.Model.ColorTransform;

public class Gray implements IPixelTransform {

	private static double srednia = 0;

	@Override
	public double[] transform(double[] pixel) {
		srednia = 0;
		for (double color : pixel)
			srednia += color / pixel.length;
		return new double[] { srednia, srednia, srednia };
	}

}
