package pl.horbo.Program2.Model.ColorTransform;

public class Sepia implements IPixelTransform {
	private int w = 20;
	private static Gray gray = new Gray();
	private static double[] result = new double[3];
	private static double[] grayPixel = new double[3];

	public Sepia(int w) throws OutOfRangeException {
		if (w < 20 || w > 40) {
			throw new OutOfRangeException(w, 20, 40);
		}
		this.w = w;
	}

	public Sepia() {
	}

	@Override
	public double[] transform(double[] pixel) {
		grayPixel = gray.transform(pixel);
		result[0] = grayPixel[0] + 2 * w;
		result[1] = grayPixel[1] + w;
		result[2] = grayPixel[2];
		return result;
	}

}
