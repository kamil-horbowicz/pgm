package pl.horbo.Program2.Model.ColorTransform;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.opencv.core.Mat;

public class GrayScale implements IColorTransform {

	private int scale;
	private boolean prompt = false;
	private JFrame parent;

	public GrayScale(JFrame parent) {
		prompt = true;
		this.parent = parent;
	}

	public GrayScale(int scale) {
		this.scale = scale;
	}

	private int getScale() throws OutOfRangeException {
		String result = JOptionPane.showInputDialog(parent, "Input scale");
		int scale = Integer.parseInt(result);
		if (scale > 8 || scale < 0) {
			throw new OutOfRangeException(scale, 0, 8);
		}
		return scale;
	}

	@Override
	public Mat transform(Mat mat) {
		if (prompt) {
			boolean error = true;
			while (error) {
				try {
					scale = getScale();
					error = false;
				} catch (OutOfRangeException e) {
					error = true;
					JOptionPane.showMessageDialog(parent, "Poza skala ([0, 8])");
				}
			}
		}

		int[] lutTable = new int[256];
		int b = 8;
		int delta = (int) (Math.pow(2, b) / Math.pow(2, scale));
		for (int i = 0; i < 256; i++) {
			lutTable[i] = (int) Math.floor(Math.max((i - (delta / 2) - 1) / delta, 0) * delta + (delta / 2) - 1);
			if (lutTable[i] > 255) {
				lutTable[i] = 255;
			}
		}

		Mat source = mat;
		Mat destination = new Mat(source.rows(), source.cols(), source.type());

		for (int i = 0; i < source.height(); i++) {
			for (int j = 0; j < source.width(); j++) {
				double[] tab = source.get(i, j);
				int pixelValue = (int) tab[0];
				tab[0] = lutTable[pixelValue];
				destination.put(i, j, tab);
			}
		}
		return destination;
	}
}
