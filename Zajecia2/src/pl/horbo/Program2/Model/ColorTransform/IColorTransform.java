package pl.horbo.Program2.Model.ColorTransform;

import org.opencv.core.Mat;

public interface IColorTransform {
	Mat transform(Mat mat);
}
