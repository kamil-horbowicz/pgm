package pl.horbo.Program2.Model.PositionTransform;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.opencv.core.Mat;

public class Move implements IPositionTransform {

	public enum Direction {
		TOP, RIGHT, DOWN, LEFT;
	}

	private Direction direction;
	private int value;
	private boolean prompt = false;
	private JFrame parent;

	public Move(int value, Direction direction) {
		this.direction = direction;
		this.value = value;
	}

	public Move(JFrame parent) {
		prompt = true;
		this.parent = parent;
	}

	private void getData() {
		this.value = Integer.parseInt(JOptionPane.showInputDialog(parent, "Input pixels"));
		this.direction = Direction.valueOf(Direction.class,
				JOptionPane.showInputDialog(parent, "Direction, type TOP, RIGHT, DOWN or LEFT"));
	}

	@Override
	public Mat transform(Mat mat) {
		if (prompt)
			getData();

		switch (direction) {
		case TOP:
			return moveTop(mat);
		case RIGHT:
			return moveRight(mat);
		case DOWN:
			return moveDown(mat);
		case LEFT:
			return moveLeft(mat);
		}
		return mat;
	}

	private int movePoint(int value, int size, int offset) {
		return Math.floorMod(value + offset, size);
	}

	private Mat moveTop(Mat mat) {
		Mat result = new Mat(mat.size(), mat.type());
		int width = (int) mat.size().width;
		int height = (int) mat.size().height;
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				result.put(movePoint(row, height, -value), col, mat.get(row, col));
			}
		}
		return result;
	}

	private Mat moveRight(Mat mat) {
		Mat result = new Mat(mat.size(), mat.type());
		int width = (int) mat.size().width;
		int height = (int) mat.size().height;
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				result.put(row, movePoint(col, width, value), mat.get(row, col));
			}
		}
		return result;
	}

	private Mat moveDown(Mat mat) {
		Mat result = new Mat(mat.size(), mat.type());
		int width = (int) mat.size().width;
		int height = (int) mat.size().height;
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				result.put(movePoint(row, height, value), col, mat.get(row, col));
			}
		}
		return result;
	}

	private Mat moveLeft(Mat mat) {
		Mat result = new Mat(mat.size(), mat.type());
		int width = (int) mat.size().width;
		int height = (int) mat.size().height;
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				result.put(row, movePoint(col, width, -value), mat.get(row, col));
			}
		}
		return result;
	}

}
