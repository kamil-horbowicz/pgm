package pl.horbo.Program2.Model.PositionTransform;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.opencv.core.Mat;

public class Rotation implements IPositionTransform {

	private double angle;
	private int width;
	private int height;
	private boolean prompt = false;
	private JFrame parent;

	public Rotation(double angle) {
		this.angle = Math.toRadians(-angle - 90);
	}

	public Rotation(JFrame parent) {
		prompt = true;
		this.parent = parent;
	}

	private double getAngle() {
		String result = JOptionPane.showInputDialog(parent, "Input angle");
		return Double.parseDouble(result);
	}

	@Override
	public Mat transform(Mat mat) {
		if (prompt) {
			angle = Math.toRadians(-getAngle() - 90);
		}
		width = mat.width();
		height = mat.height();
		Mat result = new Mat(mat.size(), mat.type());

		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				Point<Integer> newPoint = getNewPoint(new Point<Integer>(col, row), angle);

				if (isOutOfRange(newPoint))
					continue;

				result.put(newPoint.getX(), newPoint.getY(), mat.get(row, col));
			}
		}
		return result;
	}

	private Point<Integer> getNewPoint(Point<Integer> point, double angle) {

		Point<Integer> moved = toCenter(point);
		int x = moved.getX();
		int y = moved.getY();

		double x1 = (x * Math.cos(angle) - y * Math.sin(angle));
		double y1 = (x * Math.sin(angle) + y * Math.cos(angle));

		return toBegin(new Point<Integer>((int) Math.round(x1), (int) Math.round(y1)));
	}

	private Point<Integer> toCenter(Point<Integer> point) {
		return new Point<Integer>(point.getX() - width / 2, point.getY() - height / 2);
	}

	private Point<Integer> toBegin(Point<Integer> point) {
		return new Point<Integer>(point.getX() + width / 2, point.getY() + height / 2);
	}

	private boolean isOutOfRange(Point<Integer> point) {
		if (point.getX() > width || point.getX() < 0)
			return true;
		if (point.getY() > height || point.getY() < 0)
			return true;
		return false;
	}

}
