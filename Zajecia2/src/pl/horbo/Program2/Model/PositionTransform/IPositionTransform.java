package pl.horbo.Program2.Model.PositionTransform;

import org.opencv.core.Mat;

public interface IPositionTransform {
	Mat transform(Mat mat);
}
