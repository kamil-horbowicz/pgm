package pl.horbo.Program2.View;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import pl.horbo.Program2.Model.ImageEditor;
import pl.horbo.Program2.Model.ColorTransform.BoostColors;
import pl.horbo.Program2.Model.ColorTransform.Gray;
import pl.horbo.Program2.Model.ColorTransform.GrayScale;
import pl.horbo.Program2.Model.ColorTransform.IColorTransform;
import pl.horbo.Program2.Model.ColorTransform.IPixelTransform;
import pl.horbo.Program2.Model.ColorTransform.Negative;
import pl.horbo.Program2.Model.ColorTransform.OutOfRangeException;
import pl.horbo.Program2.Model.ColorTransform.Sepia;
import pl.horbo.Program2.Model.PositionTransform.IPositionTransform;
import pl.horbo.Program2.Model.PositionTransform.Move;
import pl.horbo.Program2.Model.PositionTransform.Rotation;
import pl.horbo.Tools.ImageReader;

public class PictureWindow extends JFrame {
	private static final long serialVersionUID = -7104581532348855434L;

	private ImageEditor imageEditor;
	private ImageReader imageReader;
	private ImageIcon image;
	private JLabel imageLabel;
	private String imageSrc;

	public PictureWindow(String imageSrc) {
		super();
		this.imageSrc = imageSrc;
		setTitle(imageSrc);
		setLayout(new FlowLayout());
		setSize(600, 600);
		setVisible(true);
		imageLabel = new JLabel();
		imageLabel.setBounds(0, 0, 600, 600);
		try {
			addTransformButton(new Gray(), "Gray");
			addTransformButton(new Negative(), "Negative");
			addTransformButton(new Sepia(30), "Sepia");
			addTransformButton(new Move(this), "Move");
			addTransformButton(new Rotation(this), "Rotate");
			addTransformButton(new GrayScale(this), "GrayScale2");
			addTransformButton(new BoostColors(50), "BoostColors");
		} catch (OutOfRangeException e) {
			e.printStackTrace();
		}
		imageReader = new ImageReader(imageSrc);
		imageEditor = new ImageEditor();
		imageEditor.setReader(imageReader);
		loadFile();
		displayImage();

	}

	private void addTransformButton(IColorTransform transform, String name) {
		JButton transformColor = new JButton(name);
		transformColor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				imageEditor.transformPicture(transform);
				image = new ImageIcon(imageReader.getPhoto(imageEditor.getCurrentImage()));
				displayImage();
			}
		});
		add(transformColor);
	}

	private void addTransformButton(IPixelTransform transform, String name) {
		JButton transformColor = new JButton(name);
		transformColor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				imageEditor.transformPicture(transform);
				image = new ImageIcon(imageReader.getPhoto(imageEditor.getCurrentImage()));
				displayImage();
			}
		});
		add(transformColor);
	}

	private void addTransformButton(IPositionTransform transform, String name) {
		JButton transformColor = new JButton(name);
		transformColor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				imageEditor.transformPicture(transform);
				image = new ImageIcon(imageReader.getPhoto(imageEditor.getCurrentImage()));
				displayImage();
			}
		});
		add(transformColor);
	}

	private void loadFile() {
		image = imageReader.getImageIcon(imageSrc);
		imageEditor.setReader(imageReader);
	}

	public void displayImage() {
		imageLabel.setIcon(image);
		SwingUtilities.updateComponentTreeUI(this);
		invalidate();
		validate();
		repaint();
		add(imageLabel);
	}
}
