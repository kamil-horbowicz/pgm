package pl.horbo.Program2.View;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import pl.horbo.Program2.Model.ColorTransform.OutOfRangeException;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	private JFrame thisWindow;

	private JButton addFileBtn;
	private JFileChooser fileChooser;

	public MainWindow() throws OutOfRangeException {
		super("Editor");
		setLayout(new FlowLayout());
		setSize(600, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addOpenFileButton();
	}

	private void addOpenFileButton() {
		addFileBtn = new JButton("Open File");
		fileChooser = new JFileChooser("D:/");
		thisWindow = this;
		addFileBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = fileChooser.showOpenDialog(thisWindow);

				if (result == JFileChooser.APPROVE_OPTION) {
					new PictureWindow(fileChooser.getSelectedFile().getPath());
				}
			}
		});

		add(addFileBtn);
	}

}
