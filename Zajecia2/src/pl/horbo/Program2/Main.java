package pl.horbo.Program2;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.opencv.core.Core;

import pl.horbo.Program2.Model.ColorTransform.OutOfRangeException;
import pl.horbo.Program2.View.MainWindow;

public class Main {

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	public static void main(String[] args) throws OutOfRangeException, ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		// String img1Path = "D:/LENA_512.jpg";
		//
		// ImageReader reader = new ImageReader(img1Path);
		// ImageEditor editor = new ImageEditor(reader.getMat());
		//
		// editor.transformPostion(new Move(100, Direction.RIGHT));
		// // editor.transformColor(new Sepia(20));
		// editor.displayImage();

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					new MainWindow();
				} catch (OutOfRangeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}
}