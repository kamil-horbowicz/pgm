package pl.horbo.Program3;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class Photo {

	private static void saveImage(String path, Mat mat) {
		System.out.printf("%s %s\n", path, Highgui.imwrite(path, mat) ? "zapisany" : "blad");
	}

	private static final String zrodlo = "D:/Grafika/";
	private static final String path_to_lena = zrodlo + "LENA_512.jpg";
	private static final String path_to_lake = zrodlo + "lake.jpg";
	private static final String path_to_eagle = zrodlo + "eagle.jpg";
	private static final String path_to_zamek = zrodlo + "boldt.jpg";
	private static final String path_to_deszcz = zrodlo + "rain.jpg";
	private static final String path_to_s1 = zrodlo + "s1.png";
	private static final String path_to_s2 = zrodlo + "s2.png";
	private static final String path_to_krol1 = zrodlo + "krol1.png";
	private static final String path_to_krol2 = zrodlo + "krol2.png";
	private static final String path_to_okrag = zrodlo + "ok.jpg";

	private static void zwieksz_kolory(Mat image1, double r, double g, double b, String path_to_save) {
		Mat image2 = image1.clone();
		for (int i = 0; i < image1.size().height; i++) {
			for (int j = 0; j < image1.size().width; j++) {
				double[] data = image1.get(i, j);
				if (data[0] + b > 255)
					data[0] = 255;
				else if (data[0] + b < 0)
					data[0] = 0;
				else
					data[0] += b;
				if (data[1] + g > 255)
					data[1] = 255;
				else if (data[1] + g < 0)
					data[1] = 0;
				else
					data[1] += g;
				if (data[2] + r > 255)
					data[2] = 255;
				else if (data[2] + r < 0)
					data[2] = 0;
				else
					data[2] += r;
				image2.put(i, j, data);
			}

		}
		saveImage(path_to_save, image2);
	}

	private static void dodj_obrazy(Mat image1, Mat image2, double W1, String path_to_save) {
		double maks = 256;
		Mat image3 = image1.clone();
		for (int i = 0; i < image3.size().height; i++) {
			for (int j = 0; j < image3.size().width; j++) {
				double[] data1 = image1.get(i, j);
				double[] data2 = image2.get(i, j);
				double[] data3 = image3.get(i, j);
				for (int k = 0; k < 3; k++) {
					data1[k] = W1 * data1[k];
					data2[k] = (1 - W1) * data2[k];
					if (data1[k] + data2[k] > maks)
						data3[k] = maks;
					else
						data3[k] = (data1[k] + data2[k]) % 256;
				}

				image3.put(i, j, data3);
			}

		}
		saveImage(path_to_save, image3);
	}

	private static void odejmij_obrazy(Mat image1, Mat image2, double W1, String path_to_save) {
		Mat image3 = image1.clone();
		for (int i = 0; i < image3.size().height; i++) {
			for (int j = 0; j < image3.size().width; j++) {
				double[] data1 = image1.get(i, j);
				double[] data2 = image2.get(i, j);
				double[] data3 = image3.get(i, j);
				for (int k = 0; k < 3; k++) {
					data1[k] = W1 * data1[k];
					data2[k] = (1 - W1) * data2[k];
					if (data1[k] - data2[k] < 0)
						data3[k] = 0;
					else
						data3[k] = (data1[k] - data2[k] + 256) % 256;
				}

				image3.put(i, j, data3);
			}

		}
		saveImage(path_to_save, image3);
	}

	private static void dodj_obrazy_saturacja(Mat image1, Mat image2, double W1, String path_to_save) {
		double maks = 256;
		Mat image3 = image1.clone();
		for (int i = 0; i < image3.size().height; i++) {
			for (int j = 0; j < image3.size().width; j++) {
				double[] data1 = image1.get(i, j);
				double[] data2 = image2.get(i, j);
				double[] data3 = image3.get(i, j);
				for (int k = 0; k < 3; k++) {
					data1[k] = data1[k];
					data2[k] = (1 - W1) * data2[k];
					if (data1[k] + data2[k] > maks)
						data3[k] = maks;
					else
						data3[k] = (data1[k] + data2[k]) % 256;

				}

				image3.put(i, j, data3);
			}

		}
		saveImage(path_to_save, image3);
	}

	private static void przemnoz_obrazy(Mat image1, Mat image2, double W1, String path_to_save) {
		double maks = 256;
		Mat image3 = image1.clone();
		for (int i = 0; i < image3.size().height; i++) {
			for (int j = 0; j < image3.size().width; j++) {
				double[] data1 = image1.get(i, j);
				double[] data2 = image2.get(i, j);
				double[] data3 = image3.get(i, j);
				for (int k = 0; k < 3; k++) {
					data1[k] = W1 * data1[k];
					data2[k] = (1 - W1) * data2[k];
					if (data1[k] * (data2[k] / 255) > maks)
						data3[k] = maks;
					else
						data3[k] = data1[k] * (data2[k] / 255);
				}

				image3.put(i, j, data3);
			}

		}
		saveImage(path_to_save, image3);
	}

	private static void podziel_obrazy(Mat image1, Mat image2, double W1, String path_to_save) {
		Mat image3 = image1.clone();
		for (int i = 0; i < image3.size().height; i++) {
			for (int j = 0; j < image3.size().width; j++) {
				double[] data1 = image1.get(i, j);
				double[] data2 = image2.get(i, j);
				double[] data3 = image3.get(i, j);
				for (int k = 0; k < 3; k++) {
					data1[k] = W1 * data1[k];
					data2[k] = (1 - W1) * data2[k];
					if (data2[k] == 0)
						data3[k] = data1[k] / 0.00000001;
					else
						data3[k] = data1[k] / data2[k];
				}

				image3.put(i, j, data3);
			}

		}
		saveImage(path_to_save, image3);
	}

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat lena = Highgui.imread(path_to_lena);
		Mat lake = Highgui.imread(path_to_lake);
		Mat eagle = Highgui.imread(path_to_eagle);
		Mat zamek = Highgui.imread(path_to_zamek);
		Mat deszcz = Highgui.imread(path_to_deszcz);
		Mat s1 = Highgui.imread(path_to_s1);
		Mat s2 = Highgui.imread(path_to_s2);
		Mat krol1 = Highgui.imread(path_to_krol1);
		Mat krol2 = Highgui.imread(path_to_krol2);
		Mat okrag = Highgui.imread(path_to_okrag);

		String folder_wynik = "D:/Grafika/wynik/";
		String lena_kolory_path = folder_wynik + "01_LENA_kolory.jpg";
		String eagle_lake_path = folder_wynik + "02_lena_eagle.jpg";
		String zamek_deszcz_saturacja = folder_wynik + "03_saturacja.jpg";
		String eagle_lake_path_odejmowanie = folder_wynik + "05_lena_eagle_odejmowanie.jpg";
		String roznica_pierwsza = folder_wynik + "06a_roznica_pierwsza.png";
		String roznica_druga = folder_wynik + "06b_roznica_druga.png";
		String mnozenie_okrag_lena = folder_wynik + "07_mnozenie_okrag_lena.jpg";
		String dzielenie_jezioro_orzel = folder_wynik + "08_dzielenie_jezioro_okrag.jpg";
		String roznica_dzielenie1 = folder_wynik + "09_roznica_dzielenie1.png";
		String roznica_dzielenie2 = folder_wynik + "09_roznica_dzielenie2.png";

		zwieksz_kolory(lena, -100, 100, 4400, lena_kolory_path);
		dodj_obrazy(lake, eagle, 0.5, eagle_lake_path);
		dodj_obrazy_saturacja(zamek, deszcz, 0.2, zamek_deszcz_saturacja);
		odejmij_obrazy(lake, eagle, 0.5, eagle_lake_path_odejmowanie);
		odejmij_obrazy(s1, s2, 0.6, roznica_pierwsza);
		odejmij_obrazy(krol1, krol2, 0.58, roznica_druga);
		przemnoz_obrazy(okrag, lena, 0.6, mnozenie_okrag_lena);
		podziel_obrazy(lake, eagle, 0.6, dzielenie_jezioro_orzel);
		podziel_obrazy(s1, s2, 0.9, roznica_dzielenie1);
		podziel_obrazy(krol1, krol2, 0.97, roznica_dzielenie2);
	}

}
