package pl.horbo.Program4;

enum Mats {
	COLOR("COLOR"), BW("BW"), UNIFORMCOLOR("UNI"), UNIFORMBW("UNI_BW"), NORMALCOLOR("NOR"), NORMALBW(
			"NOR_BW"), SALTCOLOR("SAL"), SALTBW("SAL_BW"), AVGUNI("UNI_BW_FILTER_AVG"), AVGNORM(
					"NOR_BW_FILTER_AVG"), AVGSALT("SAL_BW_FILTER_AVG"), MEDUNI("UNI_BW_FILTER_MED"), MEDNORM(
							"NOR_BW_FILTER_MED"), MEDSALT("SAL_BW_FILTER_MED");

	private String name;

	Mats(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}