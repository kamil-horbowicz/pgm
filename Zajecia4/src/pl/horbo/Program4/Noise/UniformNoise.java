package pl.horbo.Program4.Noise;

import java.util.Random;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Klasa s�u��ca do generowania szumu sta�ego.
 * 
 * @author Kamil Horbowicz
 */
public class UniformNoise {

	private Mat source;
	private Mat target;
	private boolean blackAndWhite = false;

	public UniformNoise(Mat source) {
		this.source = source;
	}

	public Mat getSource() {
		return source;
	}

	public void setSource(Mat source) {
		this.source = source;
	}

	/**
	 * Pobiera macierz wynikow�.
	 * 
	 * @return Macierz wynikowa. NULL, gdy nigdy nie wykonano metody
	 *         <b>transform()</b>
	 */
	public Mat getTarget() {
		return target;
	}

	/**
	 * Ustawia docelow� macierz na czarnobia��. Nale�y ponownie wykona� metod�
	 * <b>transform()</b>.
	 *
	 */
	public void setBlackWhite() {
		blackAndWhite = true;
	}

	/**
	 * Ustawia macierz wynikow� jako kolorow� (RGB). Ustawienie domy�lne.
	 */
	public void setColor() {
		blackAndWhite = false;
	}

	/**
	 * Wykonuje algorytm szumu. Macierz wynikow� nale�y pobra� za pomoc� metody
	 * <b>getTarget()</b>
	 * 
	 * @param level
	 *            - Maksymalny poziom wzburzenia.
	 * @param probability
	 *            - Prawdopodobie�stwo wyst�piena wzburzenia.
	 */
	public void transform(int level, double probability) {
		Random r = new Random();
		if (blackAndWhite) {
			Imgproc.cvtColor(source, target, Imgproc.COLOR_BGR2GRAY);
		} else {
			target = source.clone();
		}

		for (int row = 0; row < source.rows(); ++row) {
			for (int col = 0; col < source.cols(); ++col) {
				if (r.nextDouble() < probability) {
					int a = r.nextInt(2 * level);
					a -= level;
					if (a == 0)
						a = level;
					double[] RGB = new double[blackAndWhite ? 1 : 3];
					RGB = source.get(row, col);
					for (int i = 0; i < RGB.length; i++) {
						RGB[i] = a;
					}
					target.put(row, col, RGB);
				}
			}
		}
	}
}
