package pl.horbo.Program4.Noise;

import java.util.Random;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Klasa s�u��ca do generowania szumu typu s�l i pieprz.
 * 
 * @author Kamil Horbowicz
 */
public class SaltPepperNoise {

	private Mat source;
	private Mat target;
	private boolean blackAndWhite = false;

	/**
	 * 
	 * @param source
	 *            Macierz �r�d�owa na kt�rej b�dzie operowa� algorytm.
	 */
	public SaltPepperNoise(Mat source) {
		this.source = source;
	}

	/**
	 * 
	 * @return Macierz �r�d�owa algorytmu.
	 * 
	 */
	public Mat getSource() {
		return source;
	}

	/**
	 * Ustawia macierz �r�d�ow�.
	 * 
	 * @param source
	 *            Macierz �r�d�owa.
	 * 
	 */
	public void setSource(Mat source) {
		this.source = source;
	}

	/**
	 * 
	 * @return Macierz wynikowa algorytmu.
	 */
	public Mat getTarget() {
		return target;
	}

	/**
	 * Ustawia docelow� macierz na czarnobia��. Nale�y ponownie wykona� metod�
	 * <b>transform()</b>.
	 *
	 */
	public void setBlackWhite() {
		blackAndWhite = true;
	}

	/**
	 * Ustawia macierz wynikow� jako kolorow� (RGB). Ustawienie domy�lne.
	 */
	public void setColor() {
		blackAndWhite = false;
	}

	/**
	 * Wykonuje algorytm szumu. Macierz wynikow� nale�y pobra� za pomoc� metody
	 * <b>getTarget()</b>
	 * 
	 * @param probability
	 *            Prawdopodobie�stwo wyst�penia szumu.
	 */
	public void transform(double probability) {
		Random r = new Random();
		if (blackAndWhite) {
			Imgproc.cvtColor(source, target, Imgproc.COLOR_BGR2GRAY);
		} else {
			target = source.clone();
		}
		for (int row = 0; row < target.rows(); ++row) {
			for (int col = 0; col < target.cols(); ++col) {
				if (r.nextDouble() < probability) {
					int a = r.nextInt(2) * 255;
					double[] RGB = new double[blackAndWhite ? 1 : 3];
					for (int i = 0; i < RGB.length; i++) {
						RGB[i] = a;
					}
					target.put(row, col, RGB);
				}
			}
		}
	}
}
