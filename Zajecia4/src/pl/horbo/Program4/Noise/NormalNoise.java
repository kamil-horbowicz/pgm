package pl.horbo.Program4.Noise;

import java.util.Random;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Klasa s�u��ca do generowania szumu gaussowskiego.
 * 
 * @author Kamil Horbowicz
 */
public class NormalNoise {

	private Mat source;
	private Mat target;
	private boolean blackAndWhite = false;

	public NormalNoise(Mat source) {
		this.source = source;
	}

	/**
	 * Zwraca macierz �r�d�ow�.
	 * 
	 * @return Macierz �r�d�owa algorytmu.
	 * @see NormalNoise#setSource(Mat)
	 */
	public Mat getSource() {
		return source;
	}

	/**
	 * Ustawia macierz �r�d�ow�.
	 * 
	 * @param source
	 *            Macierz �r�d�owa.
	 * @see NormalNoise#getSource()
	 */
	public void setSource(Mat source) {
		this.source = source;
	}

	/**
	 * Ustawia docelow� macierz na czarnobia��. Nale�y ponownie wykona� metod�
	 * <b>transform()</b>.
	 * 
	 * @see NormalNoise#setColor()
	 * @see NormalNoise#transform(double, double, double)
	 */
	public void setBlackWhite() {
		blackAndWhite = true;
	}

	/**
	 * Ustawia macierz wynikow� jako kolorow� (RGB). Ustawienie domy�lne.
	 * 
	 * @see NormalNoise#setBlackWhite()
	 * @see NormalNoise#transform(double, double, double)
	 */
	public void setColor() {
		blackAndWhite = false;
	}

	/**
	 * 
	 * @return Macierz wynikowa algorytmu.
	 */
	public Mat getTarget() {
		return target;
	}

	/**
	 * Wykonuje algorytm szumu. Macierz wynikow� nale�y pobra� za pomoc� metody
	 * <b>getTarget()</b>
	 * 
	 * @param m
	 *            - �rednia
	 * @param s
	 *            - Odchylenie standardowe
	 * @param probability
	 *            - Prawdopodobie�stwo wyst�penia szumu.
	 */
	public void transform(double m, double s, double probability) {
		Random r = new Random();
		if (blackAndWhite) {
			Imgproc.cvtColor(source, target, Imgproc.COLOR_BGR2GRAY);
		} else {
			target = source.clone();
		}

		for (int row = 0; row < target.rows(); ++row) {
			for (int col = 0; col < target.cols(); ++col) {
				if (r.nextDouble() < probability) {
					int a = (int) NormalRand.next(m, s);
					double[] RGB = target.get(row, col);
					for (int i = 0; i < RGB.length; i++) {
						RGB[i] += a;
					}
					target.put(row, col, RGB);
				}
			}
		}
	}
}
