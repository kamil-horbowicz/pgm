package pl.horbo.Program4.Noise;

import java.util.Random;

/**
 * 
 * @author Kamil Horbowicz
 *
 */
public class NormalRand {
	private static Random random = new Random();

	/**
	 * 
	 * @param m
	 *            - �rednia
	 * @param s
	 *            - Odchylenie standardowe
	 * @return Losowa warto�� rozk�adu normalnego o podanych parametrach
	 *         <b>m</b> i <b>s</b>.
	 */
	public static double next(double m, double s) {
		double wynik;
		wynik = random.nextGaussian();
		wynik += m;
		wynik *= s;
		return wynik;
	}
}
