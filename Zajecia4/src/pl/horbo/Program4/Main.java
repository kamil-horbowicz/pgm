package pl.horbo.Program4;

import java.io.IOException;

import org.opencv.core.Core;

import pl.horbo.Program4.Filter.AvgFilter;
import pl.horbo.Program4.Filter.MedFilter;
import pl.horbo.Program4.Noise.NormalNoise;
import pl.horbo.Program4.Noise.SaltPepperNoise;
import pl.horbo.Program4.Noise.UniformNoise;
import pl.horbo.Program4.Tools.CompareMat;
import pl.horbo.Tools.HMath;
import pl.horbo.Tools.ImageReaderWriter;
import pl.horbo.Tools.Pictures;

public class Main {

	public static void main(String[] args) throws IOException {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		ImageReaderWriter irw = new ImageReaderWriter();
		RCont c = new RCont();

		c.put(Mats.COLOR, Pictures.LENA.getMat());
		c.put(Mats.BW, irw.readBWMatrix(Pictures.LENA.getPath()));

		UniformNoise uniformNoise = new UniformNoise(c.get(Mats.COLOR));
		{
			uniformNoise.transform(20, 0.3);
			c.put(Mats.UNIFORMCOLOR, uniformNoise.getTarget());

			uniformNoise.setBlackWhite();
			uniformNoise.transform(20, 0.3);
			c.put(Mats.UNIFORMBW, uniformNoise.getTarget());
		}

		NormalNoise normalNoise = new NormalNoise(c.get(Mats.COLOR));
		{
			normalNoise.transform(5, 3, 0.3);
			c.put(Mats.NORMALCOLOR, normalNoise.getTarget());

			normalNoise.setBlackWhite();
			normalNoise.transform(5, 3, 0.3);
			c.put(Mats.NORMALBW, normalNoise.getTarget());
		}

		SaltPepperNoise saltPepperNoise = new SaltPepperNoise(c.get(Mats.COLOR));
		{
			saltPepperNoise.transform(0.3);
			c.put(Mats.SALTCOLOR, saltPepperNoise.getTarget());

			saltPepperNoise.setBlackWhite();
			saltPepperNoise.transform(0.3);
			c.put(Mats.SALTBW, saltPepperNoise.getTarget());
		}

		AvgFilter avgFilter = new AvgFilter(null);
		{
			avgFilter.setSource(c.get(Mats.UNIFORMBW));
			avgFilter.transform();
			c.put(Mats.AVGUNI, avgFilter.getTarget());

			avgFilter.setSource(c.get(Mats.NORMALBW));
			avgFilter.transform();
			c.put(Mats.AVGNORM, avgFilter.getTarget());

			avgFilter.setSource(c.get(Mats.SALTBW));
			avgFilter.transform();
			c.put(Mats.AVGSALT, avgFilter.getTarget());

		}

		MedFilter medFilter = new MedFilter(null);
		{
			medFilter.setSource(c.get(Mats.UNIFORMBW));
			medFilter.transform();
			c.put(Mats.MEDUNI, medFilter.getTarget());

			medFilter.setSource(c.get(Mats.NORMALBW));
			medFilter.transform();
			c.put(Mats.MEDNORM, medFilter.getTarget());

			medFilter.setSource(c.get(Mats.SALTBW));
			medFilter.transform();
			c.put(Mats.MEDSALT, medFilter.getTarget());
		}

		{

			long uniAVG = CompareMat.compare(c.get(Mats.BW), c.get(Mats.AVGUNI));
			long normalAVG = CompareMat.compare(c.get(Mats.BW), c.get(Mats.AVGNORM));
			long saltAVG = CompareMat.compare(c.get(Mats.BW), c.get(Mats.AVGSALT));

			long uniMED = CompareMat.compare(c.get(Mats.BW), c.get(Mats.MEDUNI));
			long normalMED = CompareMat.compare(c.get(Mats.BW), c.get(Mats.MEDNORM));
			long saltMED = CompareMat.compare(c.get(Mats.BW), c.get(Mats.MEDSALT));

			long[] UNI = new long[] { uniAVG, uniMED };
			long[] NOR = new long[] { normalMED, normalAVG };
			long[] SAL = new long[] { saltAVG, saltMED };

			String text = "%s filtered by %s:\t%d\n";
			String textBest = "%s filtered by %s:\t%d\tBEST\n";

			String uni = "Uni";
			String gauss = "Gau";
			String salt = "S&P";
			String avgF = "AVG";
			String medF = "MED";

			System.out.printf(HMath.isMin(uniAVG, UNI) ? textBest : text, uni, avgF, uniAVG);
			System.out.printf(HMath.isMin(uniMED, UNI) ? textBest : text, uni, medF, uniMED);
			System.out.println();
			System.out.printf(HMath.isMin(normalAVG, NOR) ? textBest : text, gauss, avgF, normalAVG);
			System.out.printf(HMath.isMin(normalMED, NOR) ? textBest : text, gauss, medF, normalMED);
			System.out.println();
			System.out.printf(HMath.isMin(saltAVG, SAL) ? textBest : text, salt, avgF, saltAVG);
			System.out.printf(HMath.isMin(saltMED, SAL) ? textBest : text, salt, medF, saltMED);

			c.results.entrySet().forEach(mat -> irw.saveMat(mat.getValue(), mat.getKey().toString() + ".jpg"));

		}

	}

}
