package pl.horbo.Program4;

import java.util.HashMap;
import java.util.Map;

import org.opencv.core.Mat;

public class RCont {
	Map<Mats, Mat> results = new HashMap<>();

	public void put(Mats mats, Mat mat) {
		results.put(mats, mat.clone());
	}

	public Mat get(Mats mats) {
		return results.get(mats);
	}
}