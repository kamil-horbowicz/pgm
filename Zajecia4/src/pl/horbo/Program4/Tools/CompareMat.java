package pl.horbo.Program4.Tools;

import org.opencv.core.Mat;

/**
 * Klasa s�u��ca do por�wnywana macierzy.
 * 
 * @author Kamil Horbowicz
 * @version 0.0.1
 */
public class CompareMat {
	/**
	 * Por�wnuje dwie macierze. Im mniej r�ni�cych si� p�l macierzy (i o mniej
	 * si� r�ni�) tym wynik metody jest mniejszy, mniej - lepiej.
	 * 
	 * @param mat1
	 *            Macierz A
	 * @param mat2
	 *            Macierz B
	 * @return Wynik dzia�ania metody. Im mniejszy tym mniejsza r�nica mi�dzy
	 *         obrazami.
	 */
	public static long compare(Mat mat1, Mat mat2) {
		long wynik = 0;
		for (int i = 1; i < mat1.cols() - 1; ++i)
			for (int j = 1; j < mat1.rows() - 1; ++j) {
				wynik += (long) Math.abs(mat1.get(i, j)[0] - mat2.get(i, j)[0]);
			}
		return wynik;
	}
}
