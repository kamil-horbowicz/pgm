package pl.horbo.Program4.Filter;

import org.opencv.core.Mat;

public class AvgFilter {

	private Mat source;
	private Mat target;

	public AvgFilter(Mat source) {
		this.source = source;
	}

	public Mat getSource() {
		return source;
	}

	public void setSource(Mat source) {
		this.source = source;
	}

	public Mat getTarget() {
		return target;
	}

	public void transform() {
		double[] srednia = new double[3];
		target = source.clone();
		for (int i = 1; i < source.rows() - 1; ++i)
			for (int j = 1; j < source.cols() - 1; ++j) {

				for (int n = 0; n < source.channels(); n++) {
					srednia[n] = (source.get(i - 1, j - 1)[n] + source.get(i - 1, j)[n] + source.get(i - 1, j + 1)[n]
							+ source.get(i, j - 1)[n] + source.get(i, j)[n] + source.get(i, j + 1)[n]
							+ source.get(i + 1, j - 1)[n] + source.get(i + 1, j)[n] + source.get(i + 1, j + 1)[n]) / 9;
				}
				target.put(i, j, srednia);
			}
	}

	public void transform(int color) {
		double[] srednia = new double[3];
		target = source.clone();
		for (int i = 1; i < source.cols() - 1; ++i)
			for (int j = 1; j < source.rows() - 1; ++j) {

				srednia[rest(color)[0]] = source.get(j, i)[rest(color)[0]];
				srednia[rest(color)[1]] = source.get(j, i)[rest(color)[1]];
				srednia[color] = (source.get(i - 1, j - 1)[color] + source.get(i - 1, j)[color]
						+ source.get(i - 1, j + 1)[color] + source.get(i, j - 1)[color] + source.get(i, j)[color]
						+ source.get(i, j + 1)[color] + source.get(i + 1, j - 1)[color] + source.get(i + 1, j)[color]
						+ source.get(i + 1, j + 1)[color]) / 9;

				target.put(i, j, srednia);
			}
	}

	private byte[] rest(int color) {
		switch (color) {
		case 0:
			return new byte[] { 1, 2 };
		case 1:
			return new byte[] { 0, 2 };
		case 2:
			return new byte[] { 0, 1 };
		default:
			return null;
		}
	}
}
