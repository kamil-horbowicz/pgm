package pl.horbo.Program4.Filter;

import java.util.Arrays;

import org.opencv.core.Mat;

public class MedFilter {

	private Mat source;
	private Mat target;

	public MedFilter(Mat source) {
		super();
		this.source = source;
	}

	public Mat getSource() {
		return source;
	}

	public void setSource(Mat source) {
		this.source = source;
	}

	public Mat getTarget() {
		return target;
	}

	public void transform() {
		target = source.clone();
		double[] srednia = new double[9];
		for (int i = 1; i < source.cols() - 1; ++i) {
			for (int j = 1; j < source.rows() - 1; ++j) {

				for (int nr = 0; nr < source.channels(); ++nr) {
					for (int k = 0; k < 9; ++k) {
						srednia[k] = source.get(i - 1 + k / 3, j - 1 + k % 3)[nr];
					}
					Arrays.sort(srednia);
					double[] out = new double[3];
					out = source.get(i, j);
					out[nr] = srednia[4];
					target.put(i, j, out);
				}
			}
		}
	}

}
